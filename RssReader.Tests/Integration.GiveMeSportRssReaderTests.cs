﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace RssReader.Tests.Integration
{
    [TestClass]
    public class GiveMeSportRssReaderTests
    {
        [TestMethod]
        [TestCategory("Integration")]
        public void ReadFeedExpectResultSortedWithTenItems()
        {
            var target = new GiveMeSportRssReader();

            var result = target.Read();

            Assert.AreEqual(10, result.Count);
            Assert.IsTrue(result.First().PublishDate > result.Last().PublishDate);
        }
    }
}
