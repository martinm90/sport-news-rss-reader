﻿using HigLabo.Net.Rss;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Linq;

namespace RssReader.Tests
{
    [TestClass]
    public class GiveMeSportRssReaderTests
    {
        private IGiveMeSportRssReader _target;
        private RssFeed _feed;

        public GiveMeSportRssReaderTests()
        {
            string xmlFile = File.ReadAllText(@"TestData\TestGiveMeSportRssFeed.xml");
            _feed = new RssFeed(xmlFile);

            _target = new GiveMeSportRssReader();
        }

        [TestMethod]
        public void SortExpectResultSortedWithTenItems()
        {
            var result = _target.Sort(_feed);

            Assert.AreEqual(10, result.Count);
            Assert.IsTrue(result.First().PublishDate > result.Last().PublishDate);
        }

        [TestMethod]
        public void SortExpectResultSortedWithValuesInRequiredProperties()
        {
            var result = _target.Sort(_feed);

            Assert.AreEqual(10, result.Count);

            foreach (var item in result)
            {
                Assert.IsNotNull(item.Link);
                Assert.IsNotNull(item.Title);
                Assert.IsNotNull(item.PublishDate);
            }
        }

        [TestMethod]
        public void SortWithEmptyFeedExpectEmptyList()
        {
            var feed = new RssFeed(string.Empty);
            var result = _target.Sort(feed);

            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);
        }
    }
}
