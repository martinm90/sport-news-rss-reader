﻿using HtmlAgilityPack;
using System.Drawing;
using System.IO;
using System.Net;

namespace GiveMeSport.GiveMeSportPageReader
{
    public interface IHtmlPageReader
    {
        string GetImageUrl(string pageUrl);
    }

    public class HtmlPageReader : IHtmlPageReader
    {
        private const string IMAGE_ID = "EdImg";
        private const string ATTRIBUTE_NAME = "src";

        public string GetImageUrl(string pageUrl)
        {
            HtmlWeb htmlWeb = new HtmlWeb();

            HtmlAgilityPack.HtmlDocument document = htmlWeb.Load(pageUrl);

            var someNode = document.GetElementbyId(IMAGE_ID).Attributes[ATTRIBUTE_NAME].Value;
            return (string)someNode;
        }
    }
}