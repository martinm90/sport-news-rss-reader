﻿using GiveMeSport.GiveMeSportPageReader;
using Microsoft.Practices.Unity;

namespace GiveMeSport
{
    public class UnityBootstrapper
    {
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IHtmlPageReader, HtmlPageReader>();
        }
    }
}
