﻿using GiveMeSport.GiveMeSportPageReader;
using GiveMeSport.ViewModels;
using RssReader;
using RssReader.Model;
using System.Collections.Generic;
using System.Web.Mvc;

namespace GiveMeSport.Controllers
{
    public class HomeController : Controller
    {
        private IGiveMeSportRssReader _rssReader;
        private IHtmlPageReader _htmlReader;

        public HomeController(IGiveMeSportRssReader rssReader, IHtmlPageReader htmlReader)
        {
            _rssReader = rssReader;
            _htmlReader = htmlReader;
        }

        public ActionResult Index()
        {
            var rssItems = ToRssItemViewModels(_rssReader.Read());
            return View(rssItems);
        }

        public ActionResult About()
        {
            ViewBag.Message = "About. My choices!";

            return View();
        }

        private IEnumerable<RssItemViewModel> ToRssItemViewModels(IEnumerable<GiveMeSportRssItem> rssItems)
        {
            foreach (var item in rssItems)
            {
                yield return ToRssItemViewModels(item);
            }
        }

        private RssItemViewModel ToRssItemViewModels(GiveMeSportRssItem rssItem)
        {
            return new RssItemViewModel
            {
                Title = rssItem.Title,
                Link = rssItem.Link,
                PublishDate = rssItem.PublishDate.ToLongDateString(),
                ImageUrl = _htmlReader.GetImageUrl(rssItem.Link),
            };
        }
    }
}