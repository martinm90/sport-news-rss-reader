﻿using System.Drawing;

namespace GiveMeSport.ViewModels
{
    public class RssItemViewModel
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public string PublishDate { get; set; }
        public string ImageUrl { get; set; }
    }
}