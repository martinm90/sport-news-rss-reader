﻿using HigLabo.Net.Rss;
using RssReader.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RssReader.Extensions
{
    public static class RssReaderExtensions
    {
        public static GiveMeSportRssItem ToGiveMeSportRssItem(this RssItem rssItem)
        {
            return new GiveMeSportRssItem
            {
                Title = rssItem.Title,
                Link = rssItem.Link,
                PublishDate = rssItem.PubDate.Value.DateTime,
            };
        }
    }
}
