﻿using System;

namespace RssReader.Model
{
    public class GiveMeSportRssItem
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public string Comment { get; set; }
        public DateTime PublishDate { get; set; }
    }
}
