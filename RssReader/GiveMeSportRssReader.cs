﻿using HigLabo.Net.Rss;
using RssReader.Extensions;
using RssReader.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RssReader
{
    public interface IGiveMeSportRssReader
    {
        IList<GiveMeSportRssItem> Read();
        IList<GiveMeSportRssItem> Sort(RssFeed feed);
    }

    public class GiveMeSportRssReader : IGiveMeSportRssReader
    {
        private const string URL = "http://www.givemesport.com/rss.ashx";
        private const int NUMBER_OF_ITEMS = 10;

        /// <summary>
        /// Reads the rss feed to get a list of RssItems
        /// </summary>
        public IList<GiveMeSportRssItem> Read()
        {
            var client = new RssClient();
            var feed = client.GetRssFeed(new Uri(URL));

            return Sort(feed);
        }

        /// <summary>
        /// Takes a RssFeed and returns the ten most recent items
        /// </summary>
        public IList<GiveMeSportRssItem> Sort(RssFeed feed)
        {
            var items = feed.Items.Select(item => item.ToGiveMeSportRssItem());

            return items.OrderByDescending(item => item.PublishDate).Take(NUMBER_OF_ITEMS).ToList();
        }
    }
}
