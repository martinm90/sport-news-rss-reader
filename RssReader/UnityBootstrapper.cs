﻿using Microsoft.Practices.Unity;

namespace RssReader
{
    public class UnityBootstrapper
    {
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IGiveMeSportRssReader, GiveMeSportRssReader>();
        }
    }
}
