﻿using GiveMeSport.Controllers;
using GiveMeSport.GiveMeSportPageReader;
using GiveMeSport.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using RssReader;
using RssReader.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GiveMeSport.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        private IGiveMeSportRssReader _rssReader;
        private IHtmlPageReader _htmlReader;
        private HomeController _target;

        public HomeControllerTest()
        {
            _rssReader = MockRepository.GenerateMock<IGiveMeSportRssReader>();
            _htmlReader = MockRepository.GenerateMock<IHtmlPageReader>();
            _target = new HomeController(_rssReader, _htmlReader);
        }

        [TestMethod]
        public void Index()
        {
            // Act
            ViewResult result = _target.Index() as ViewResult;
       
            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void IndexWithValidDataFromFeedReaderExpectMappedResult()
        {
            var returnedItems = GetTestFeedItems();
            _rssReader.Stub(r => r.Read()).IgnoreArguments().Return(GetTestFeedItems());
            _htmlReader.Stub(h => h.GetImageUrl(string.Empty)).IgnoreArguments().Return("Image url");

            var result = _target.Index() as ViewResult;
            Assert.IsTrue(result.Model is IEnumerable<RssItemViewModel>);
            var modelResult = (IEnumerable<RssItemViewModel>)result.Model;
            Assert.IsNotNull(modelResult);

            Assert.AreEqual(2, modelResult.Count());
            var firstResult = modelResult.First();
            Assert.AreEqual("This is a big news event!", firstResult.Title);
            Assert.AreEqual("link 1", firstResult.Link);
            Assert.AreEqual(DateTime.Parse("2015-01-01").ToLongDateString(), firstResult.PublishDate);
            Assert.AreEqual("Image url", firstResult.ImageUrl);
        }

        [TestMethod]
        public void IndexWithNoFeedItemsExpectNothingMapped()
        {
            _rssReader.Stub(r => r.Read()).IgnoreArguments().Return(new List<GiveMeSportRssItem>());
            _htmlReader.Stub(h => h.GetImageUrl(string.Empty)).IgnoreArguments().Return("Image url");

            var result = _target.Index() as ViewResult;
            Assert.IsTrue(result.Model is IEnumerable<RssItemViewModel>);
            var modelResult = (IEnumerable<RssItemViewModel>)result.Model;
            Assert.IsNotNull(modelResult);

            Assert.AreEqual(0, modelResult.Count());        }

        [TestMethod]
        public void About()
        {
            // Act
            ViewResult result = _target.About() as ViewResult;

            // Assert
            Assert.AreEqual("About. My choices!", result.ViewBag.Message);
        }

        private IList<GiveMeSportRssItem> GetTestFeedItems()
        {
            return new List<GiveMeSportRssItem>
            {
                new GiveMeSportRssItem
                {
                    Comment = "comment 1",
                    Link = "link 1",
                    PublishDate = DateTime.Parse("2015-01-01"),
                    Title = "This is a big news event!"
                },
                new GiveMeSportRssItem
                {
                    Comment = "comment 2",
                    Link = "link 2",
                    PublishDate = DateTime.Parse("2015-01-01"),
                    Title = "This is an even bigger news event!"
                },
            };
        }
    }
}
